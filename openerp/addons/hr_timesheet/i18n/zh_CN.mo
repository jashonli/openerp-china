��    V      �     |      x     y     �  �  �    I	     Q  
   X     c     t     �     �     �     �  3   �     �     �                  
   #  
   .     9     F     U     Z     y     �     �     �  R   �     �      �             '     7     K     i     r     �  }   �     
               .     >     V  |   \  l   �  a   F     �  *   �  %   �          	          ,     ?     V     j     r     �     �     �     �  
   �     �  	   �     �     �          !     0     @  
   S  
   ^  
   i     t     �     �     �     �     �     �  �   �       �  �     /     J  �  f    �     �     �     �          !     4     G     N  <   [     �     �     �     �     �  	   �  	   �     �     �     �                    $     +  B   4     w  �   �  �   P          *     1     >  	   Q     [     t  ~   w     �     �                 $      =   _   A   V   �   J   �      C!  !   P!  '   r!     �!     �!     �!     �!     �!     �!     �!     �!     "     "     ("     5"     B"     I"  	   P"     Z"     j"     z"     �"     �"     �"  	   �"  	   �"  	   �"     �"     �"     �"     �"  	   �"     #     #  �   *#     �#        2   "      '                                ?   D                    V   Q      =           $                  %   E   5               F   N             6   C       1   B   A   M   :   U       
   &   +       4   P       8                  9      ;                    >      T   )                     R            3   G                   @   (                             J   <          !      #   O   7   	   I   ,   -   K   *          0       /   H   .       S   L        (Keep empty for current time) (local time on the server side) <p class="oe_view_nocontent_create">
                Click to record activities.
              </p><p>
                You can register and track your workings hours by project every
                day. Every time spent on a project will become a cost in the
                analytic accounting/contract and can be re-invoiced to
                customers if required.
              </p>
             <p>
                No activity yet on this contract.
              </p><p>
                In Odoo, contracts and projects are implemented using
                analytic account. So, you can track costs and revenues to analyse
                your margins easily.
              </p><p>
                Costs will be created automatically when you register supplier
                invoices, expenses or timesheets.
              </p><p>
                Revenues will be created automatically when you create customer
                invoices. Customer invoices can be created based on sale orders
                (fixed price invoices), on timesheets (based on the work done) or
                on expenses (e.g. reinvoicing of travel costs).
              </p>
             Absent Accounting Analytic Account Analytic Journal Analytic Line Analytic account Cancel Change Work Check this field if this project manages timesheets Closing Date Company Cost Cost/Revenue Costs & Revenues Created by Created on Current Date Current Status Date Define your Analytic Structure Description Duration Employee Employee ID Employee is not created for this user. Please create one from configuration panel. Employee's Name Employees can encode their time spent on the different projects they are assigned on. A  project is an analytic account and the time spent on a project generates costs on the analytic account. This feature allows to record at the same time the attendance and the timesheet. Employees can encode their time spent on the different projects. A project is an analytic account and the time spent on a project generate costs on the analytic account. This feature allows to record at the same time the attendance and the timesheet. Extended Filters... General Account General Information Go to the configuration panel Group By Group by month of date ID If you want to reinvoice working time of employees, link this employee to a service to determinate the cost price of the job. Information Journal Last Updated by Last Updated on Minimum Analytic Amount Month No 'Analytic Journal' is defined for employee %s 
Define an employee for the selected user and assign an 'Analytic Journal'! No analytic account is defined on the project.
Please set one or we cannot automatically fill the timesheet. No analytic journal defined for '%s'.
You should assign an analytic journal on the employee form. Partner Please define cost unit for this employee. Please define employee for your user. Present Product Project / Analytic Account Sign In By Project Sign In/Out by Project Sign Out By Project Sign in Sign in / Sign out Sign in / Sign out by Project Start Working Starting Date Stop Working This Month Time Timesheet Timesheet Accounts Timesheet Activities Timesheet Analysis Timesheet Line Timesheet Month Timesheet by Month Timesheets Total cost Total time Unit of Measure User User Error! Users Warning! Work Description Work done in the last period You should create an analytic account structure depending on your needs to analyse costs and revenues. In Odoo, analytic accounts are also used to track customer contracts. month Project-Id-Version: openobject-addons
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-23 16:27+0000
PO-Revision-Date: 2014-10-05 06:54+0800
Last-Translator: 保定-粉刷匠 <992102498@qq.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-03-28 05:43+0000
X-Generator: Poedit 1.6.9
 (留空表示当前时间) （服务器上的时间） <p class="oe_view_nocontent_create">
                单击创建活动记录。
              </p><p>
                你可以注册并跟踪项目每天的工时。
                如果需要的话，每一个项目上花费的时间，
                将在会计分析或合同中记为成本，
                并可重新给客户开发票。
              </p>
             <p>
                No activity yet on this contract.
              </p><p>
                In Odoo, contracts and projects are implemented using
                analytic account. So, you can track costs and revenues to analyse
                your margins easily.
              </p><p>
                Costs will be created automatically when you register supplier
                invoices, expenses or timesheets.
              </p><p>
                Revenues will be created automatically when you create customer
                invoices. Customer invoices can be created based on sale orders
                (fixed price invoices), on timesheets (based on the work done) or
                on expenses (e.g. reinvoicing of travel costs).
              </p>
             缺勤 会计 辅助核算项 辅助核算分录 辅助核算明细 辅助核算项目 取消 修改工作 如果此项目需要管理时间表的话请选中此字段 结束日期 公司 成本 成本/利润 收支情况 创建人 创建在 当前日期 当前状态 日期 设置分析结构 描述 用时 员工 员工ID 这个用户不能创建员工。请从设置面板创建一个。 员工姓名 员工能记录在不同项目所花费的时间。
一个项目是一个辅助核算項，而且该项目所花费的时间为这辅助核算項的成本。
此功能允许记录其考勤和计工单。 员工能记录在不同项目所花费的时间。一个项目是一个辅助核算項，而且该项目所花费的时间为这辅助核算項的成本。此功能允许记录其考勤和计工单。 扩展过滤... 总账 一般信息 转到设置面板 分组按 用日期的月份分组 ID 如果你要为员工的工作时间开票，把这个员工和一个服务关联起来，用以确定工作的成本价格。 信息 分录 最后更新被 最后更新在 最低辅助核算金额 月 定义雇员没有“分析日报”%s 
定义为所选用户和雇员分配“分析日报”! 没有解析的帐户上定义项目.
请设置一个或不能自动填充时间表。 没有分析定义系统  '%s'.
你应该对雇员的形式分配分析。 业务伙伴 请定义此员工的成本单位 请为你的用户定义员工信息。 出勤 产品 项目 / 辅助核算科目 项目签入 项目签入/签出 项目签出 签入 签入/ 签出 项目用时记录 开始工作 开始日期 停止工作 本月 时间 计工单 项目计工单 项目计工单 计工单分析 计工单明细 月计工单 月计工单 计工单 总成本 总时间 计量单位 用户 用户错误！ 用户 警告！ 工作说明 最近完成的工作 为了分析成本和利润，你应该根据自己的需求创建一个分析科目结构。在Odoo中,分析可以也可以用来跟踪客户的合同。  月 